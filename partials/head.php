<!-- Meta -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta name="format-detection" content="telephone=no">

<!-- Style -->
<link rel="stylesheet" href="assets/css/style.min.css">