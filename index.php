<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Front-End Starter Kit</title>

    <!-- Head tags and stylesheets -->
    <?php include 'partials/head.php'; ?>
</head>
<body>
    <!-- Header -->
    <?php include 'partials/header.php'; ?>

    <!-- Footer -->
    <?php include 'partials/footer.php'; ?>

    <!-- Scripts -->
    <?php include 'partials/scripts.php'; ?>
</body>
</html>
